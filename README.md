# Bulldoser

Bulldoser is a CLI app that renders a list of Dose articles into templates.

## Usage
```
php bulldoser.php homepage list_of_ids.json output.html
```
This will generate a homepage using the provided list of ids and write it to `output.html`

## List of Ids
This is a json file that specifies different ids in each feed. Only needed information will be used so the same file can be used for multiple templates.

## Templates

Templates are defined in the templates folder using twig. There is a `map.json` in that directory that defines required feed sizes for the templates.
