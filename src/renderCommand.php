<?php

namespace Dose\Bulldoser\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Twig_Environment;
use Twig_Loader_Filesystem;

/**
 * Class RenderCommand
 *
 * @package Dose\Bulldoser\Command
 */
class RenderCommand extends Command
{
    /**
     * URI pattern from which the article meta json file will be fetched in sprintf format
     */
    const ARTICLE_JSON_URI_PATTERN = 'http://dose-static.s3-website-us-east-1.amazonaws.com/lists/%d/list.json';

    /**
     * Template file path pattern in sprintf format
     */
    const TEMPLATE_PATH_PATTERN = 'templates/%s.twig';

    /**
     * Promo image URI patterns in sprintf format
     */
    const PROMO_IMAGE_URI_PATTERNS = [
        'base' => '/lists/%d/promo_image.jpg',
        'hero' => '/lists/%d/promo_image_hero.jpg',
        'headline' => '/lists/%d/promo_image_headline.jpg',
        'regular' => '/lists/%d/promo_image_regular.jpg',
        'small' => '/lists/%d/promo_image_small.jpg',
        'recirculation' => '/lists/%d/promo_image_recirculation.jpg',
    ];

    const CATEGORY_LINKS = [
        1 => '/entertainment',
        2 => '/nerd',
        3 => '/style',
        4 => '/hollywood',
        5 => '/theworld',
        6 => '/animals',
        7 => '/food',
        8 => '/music',
        9 => '/throwback',
        10 => '/parents',
        11 => '/homeanddiy',
        12 => '/news',
        13 => '/health',
        14 => '/sports',
        15 => '/motors'
    ];

    /**
     * Stylised output object
     *
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * Template engine
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this
            ->setName('render')
            ->setDescription('Renders static pages from templates')
            ->addArgument('template', InputArgument::REQUIRED, 'Template name')
            ->addArgument('id_list', InputArgument::REQUIRED, 'List of ids')
            ->addArgument('output', InputArgument::REQUIRED, 'Output file')
            ->addOption('force', null, InputOption::VALUE_NONE, 'Will override output file');
    }

    /**
     * Overrides synopsis to not include the command name.
     *
     * This only applies in single command apps.
     *
     * @param bool $short
     * @return string
     */
    public function getSynopsis($short = false)
    {
        return ltrim(parent::getSynopsis($short), $this->getName());
    }

    /**
     * Execute the command
     *
     * @return null
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->twig = new Twig_Environment(new Twig_Loader_Filesystem('.'));

        $outputFile = $input->getArgument('output');

        !$input->getOption('force')
            && file_exists($outputFile)
            && $this->throwError($outputFile . ' already exists');

        $template = $input->getArgument('template');

        $this->io->section('Fetching articles');

        $articles = $this->getArticles(
            $this->filterIdsByTemplate(
                $this->getIdsFromFile(
                    $input->getArgument('id_list')
                ),
                $this->getTemplateMap($template)
            )
        );

        $this->io->section('Rendering template');

        $this->writeToFile(
            $outputFile,
            $this->twig->render(
                sprintf(self::TEMPLATE_PATH_PATTERN, $template),
                $articles
            )
        );

        $this->io->success($outputFile . ' has been generated from ' . $template);

        return null;
    }

    /**
     * Checks if the specified IDs match the map and trims excess
     *
     * @param $ids
     * @param $templateMap
     * @return array
     */
    protected function filterIdsByTemplate($ids, $templateMap)
    {
        $filteredIds = [];

        foreach($templateMap as $feed => $count) {
            if (count($ids[$feed]) < $count) {
                $this->throwError($feed . ' does not contain at least ' . $count . ' articles');
            }

            $filteredIds[$feed] = array_slice($ids[$feed], 0, $count);
        }

        return $filteredIds;
    }

    /**
     * Gets a list of IDs from a file
     *
     * @param $path
     * @return array
     */
    protected function getIdsFromFile($path)
    {
        file_exists($path) or
            $this->throwError($path . ' not found');

        $json = json_decode(
            @file_get_contents($path),
            true
        );

        if ($json === null) {
            $this->throwError($path . ' is not a valid JSON');
        }

        return $json;
    }

    /**
     * Gets template map that defines minimum required feeds for a template
     *
     * @param $template
     * @return array
     */
    protected function getTemplateMap($template)
    {
        $templates = json_decode(
            @file_get_contents('./templates/map.json'),
            true
        );

        if (!$templates[$template]) {
            $this->throwError($template . ' is not a valid template');
        }

        return $templates[$template];
    }

    /**
     * Gets total count of articles in all feeds
     *
     * @param $ids
     * @return int
     */
    protected function getIdsCount($ids)
    {
        return count(call_user_func_array('array_merge', $ids));
    }

    /**
     * Gets articles meta data and reports the progress
     *
     * @param $ids
     * @return array
     */
    protected function getArticles($ids)
    {
        $articles = [];
        $badArticles = [];

        $this->io->progressStart($this->getIdsCount($ids));

        foreach ($ids as $feed => $feedIds) {
            foreach ($feedIds as $id) {
                try {
                    $articles[$feed][] = $this->getArticle($id);
                } catch (Exception $e) {
                    $badArticles[] = $id;
                }

                $this->io->progressAdvance();
            }
        }

        $this->io->progressFinish();

        if ($badArticles) {
            $this->io->table(
                ['These articles failed to fetch'],
                array_map([$this, 'toArray'], $badArticles)
            );

            $this->throwError('Some articles failed to fetch');
        }

        return $articles;
    }

    /**
     * Gets meta data of an article
     *
     * @param $id
     * @return mixed
     * @throws Exception
     */
    protected function getArticle($id)
    {
        $json = json_decode(
            @file_get_contents(sprintf(self::ARTICLE_JSON_URI_PATTERN, $id)),
            true
        );

        if (!$json['data']) {
            throw new Exception('Article [' . $id . '] is invalid');
        }

        $json['data']['promo_images'] = $this->getPromoImages($id);
        $json['data']['permalink'] = $this->getRelativeLink($json['data']['permalink']);
        $json['data']['categories'] = $this->getCategoriesWithLinks($json['data']['categories']);

        return $json['data'];
    }

    /**
     * Gets list of promo images for an article
     *
     * @param $id
     * @return array
     */
    protected function getPromoImages($id)
    {
        $promoImages = self::PROMO_IMAGE_URI_PATTERNS;

        foreach ($promoImages as $key => $value) {
            $promoImages[$key] = sprintf($value, $id);
        }

        return $promoImages;
    }

    /**
     * Gets relative URI from abslute
     *
     * @param $URI
     * @return string
     */
    protected function getRelativeLink($URI)
    {
        return parse_url($URI, PHP_URL_PATH);
    }

    /**
     * Gets a list of categories with links
     *
     * @param $categories
     * @return array
     */
    protected function getCategoriesWithLinks($categories)
    {
        $categoriesWithLinks = [];

        foreach($categories as $category) {
            $category['link'] = self::CATEGORY_LINKS[$category['id']];
            $categoriesWithLinks[] = $category;
        }

        return $categoriesWithLinks;
    }

    /**
     * Writes content to a file
     *
     * @param $file
     * @param $content
     */
    protected function writeToFile($file, $content)
    {
        if (file_put_contents($file, $content) === false) {
            $this->throwError('Unable to write to ' . $file);
        }
    }

    /**
     * Converts a value to array
     *
     * @param $value
     * @return array
     */
    protected function toArray($value)
    {
        return [$value];
    }

    /**
     * Reports an error and stops the app
     *
     * @param $message
     */
    protected function throwError($message)
    {
        $this->io->error($message);
        exit(1);
    }
}
