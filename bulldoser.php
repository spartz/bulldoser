<?php

require __DIR__ . '/vendor/autoload.php';

use Dose\Bulldoser\App;

$application = new App();
$application->run();
